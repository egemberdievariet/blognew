<?php
/**
 * Created by PhpStorm.
 * User: Ariet
 * Date: 4/17/20
 * Time: 5:37 PM
 */

namespace App\Forms;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Components\Users\Models\RecoverUserModel;

class RecoverUserForm extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('email' , EmailType::class, [
            'label' => 'Email'
        ]);
        $builder->add('submit', SubmitType::class,[
            'label' => 'Recover'
        ]);
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => RecoverUserModel::class
        ]);
    }
}