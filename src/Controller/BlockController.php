<?php
/**
 * Created by PhpStorm.
 * User: Ariet
 * Date: 4/14/20
 * Time: 6:13 PM
 */

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class BlockController extends Controller{
    public function logoAction(){
        return $this->render('Block/logo.html.twig');
    }
    public function mainMenuAction(){
        return $this->render('Block/main-menu.html.twig');
    }
    public function mainMenuFooterAction(){
        return $this->render('Block/main-menu-footer.html.twig');
    }
}