<?php
/**
 * Created by PhpStorm.
 * User: Ariet
 * Date: 4/17/20
 * Time: 5:26 PM
 */

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\User;
use App\Forms\ChangePasswordForm;
use App\Components\Users\Models\ChangePasswordModel;

class UserController extends Controller {


    public function recoverPasswordAction( Request $request ){
        /** @var User $user */
        $user = $this->getUser();
        $userAccount = $user->getAccount();
        if(!$userAccount->getTokenRecover())
            return $this->redirectToRoute('user');

        $this->container->get('security.password_encoder');
        $changePasswordModel = new ChangePasswordModel();
        $formChangePassword = $this->createForm(ChangePasswordForm::class, $changePasswordModel);
        $formChangePassword->handleRequest($request);
        if($formChangePassword->isSubmitted() && $formChangePassword->isValid()){
            $encoder = $this->get('security.password_encoder');
            $password = $encoder->encodePassword($user, $changePasswordModel->password);
            $user->setPassword($password);
            $userAccount->setTokenRecover(null);
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            return $this->redirectToRoute('user');
        }
        return $this->render('User/security/recover.html.twig',[
            'recover_form' => $formChangePassword->createView()
        ]);
    }
}