<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Forms\CommentForm;
use App\Entity\Page;
use App\Forms\PageDeleteForm;
use App\Forms\PageForm;
use App\Forms\SearchForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class PageController extends Controller {
    public function listAction(Request $request){
        $pageRepo = $this->getDoctrine()->getRepository(Page::class);
        $pager = $request->query->get('page') ? $request->query->get('page') : 1;
        $limit = 2;
        $pages = $pageRepo->findPages($pager, $limit);
        $pager = [
            'pager' =>$pager,
            'total' =>$pageRepo->countPage(),
            'limit' =>$limit
        ];
        return $this->render('Page/list.html.twig', [
            'pages' => $pages,
            'navigator' => $pager
        ]);
    }

    public function viewAction($id, Request $request, FlashBagInterface $flashBag ){
        $flashBag->add('success', 'Article is added');
        $pageRepo = $this->getDoctrine()->getRepository(Page::class);
        $page = $pageRepo->find($id);
        if(!$page){
            throw $this->createNotFoundException('The page does not exist');
        }

        $em = $this->getDoctrine()->getManager();
        $commentForm = $this->createForm(CommentForm::class);
        $commentForm->handleRequest($request);
        if ($commentForm->isSubmitted()) {
            /**
             * @var Comment $comment
             */
            $comment = $commentForm->getData();
            $comment->setPage($page);
            $em->persist($comment);
            $em->flush();
            return $this->redirectToRoute('page_view', ['id' =>$page->getId()]);
        }
        $commentRepo = $em->getRepository(Comment::class);
        $comments = $commentRepo->findLastComments($page, 7);

        return $this->render('Page/view.html.twig', [
            'page' => $page,
            'comment_form' => $commentForm->createView(),
            'page_comments' => $comments,

        ]);
    }

    public function addAction(Request $request, FlashBagInterface $flashBag){
        $page = new Page();
        $form = $this->createForm(PageForm::class, $page);
        $form->handleRequest($request);
        if ($form->isSubmitted()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($page);
            $em->flush();
            $flashBag->add('success', 'Article is added:'. $page->getTitle());
            return $this->redirectToRoute('page_list');
        }
        return $this->render('Page/add.html.twig',[
            'form' => $form->createView()
        ]);

    }
    public function editAction($id , Request $request){
        $em =  $this->getDoctrine()->getManager();
        $repo = $em->getRepository('PageBundle:Page');
        $page  = $repo->find($id);
        if (!$page)
            return $this->redirectToRoute('page_list');

        $form = $this->createForm(PageForm::class, $page);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $em->persist($page);
            $em->flush();

            return $this->redirectToRoute('page_view', ['id' => $page->getId() ]);
        }
        return $this->render('Page/add.html.twig', [
            'form' => $form->createView()
        ]);
    }

    public  function removeAction($id , Request $request, SessionInterface $session){
        $em =  $this->getDoctrine()->getManager();
        $repo = $em->getRepository(Page::class);
        $page  = $repo->find($id);
        if (!$page)
            return $this->redirectToRoute('page_list');

        $form = $this->createForm(PageDeleteForm::class, null, [
            'delete_id' => $page->getId()
        ]);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $em->remove($page);
            $session->getFlashBag('success', 'Article is deleted:'. $page->getTitle());
            $em->flush();

            return $this->redirectToRoute('page_list' );
        }
        return $this->render(':Page:delete.html.twig', [
            'form' => $form->createView()
        ]);


    }

    public function commentsAction($id, Request $request){
        $pageRepo = $this->getDoctrine()->getRepository(Page::class);
        $page = $pageRepo->find($id);
        if(!$page){
            throw $this->createNotFoundException('The page does not exist');
        }
        $pager = $request->query->get('pager') ? $request->query->get('pager') : 1;
        $limit = 10;
        $commentRepo = $this->getDoctrine()->getRepository(Comment::class);
        $comments = $commentRepo->findComments($page, $pager, $limit);
        $pager = [
            'pager' =>$pager,
            'total' =>$commentRepo->countCommments($page),
            'limit' =>$limit
        ];

//        $em = $this->getDoctrine()->getManager();

        return $this->render(':Page:page_comments.html.twig', [
            'comments' => $comments,
            'navigator' => $pager,
            'page' => $page
        ]);
    }

    public function searchAction( Request $request ){
        $pageRepo = $this->getDoctrine()->getRepository(Page::class);
//        $pages = $pageRepo->findByWord('body');
//        dump($pages);
//        die("ok");
//
        $searchForm = $this->createForm(SearchForm::class);
        $searchForm->handleRequest($request);
        $pages = null;
        if($searchForm->isSubmitted()){
            $data = $searchForm->getData();
            $pages = $pageRepo->findByWord($data['search']);
        }
        return $this->render(':Page:search.html.twig', [
            'form' => $searchForm->createView(),
            'pages' => $pages
        ]);
    }

}