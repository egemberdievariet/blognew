<?php

namespace App\Controller;


use App\Entity\Term;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class BlocksController extends Controller{
    public function categoryAction(){
        $terms = $this->getDoctrine()->getRepository(Term::class)->findAll();
        return $this->render('Block/category-list.html.twig',[
            'terms' =>  $terms
        ]);

    }
}