<?php
/**
 * Created by PhpStorm.
 * User: Ariet
 * Date: 4/17/20
 * Time: 5:43 PM
 */

namespace App\Components\Users\Provider;


use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use App\Entity\User;

class UserProvider implements UserProviderInterface {

    private $em;

    public function __construct( EntityManager $entity_manager ) {
        $this->em = $entity_manager;
    }

    public function loadUserByUsername($username) {
        $user = $this->em->getRepository('UserBundle:User')->loadUserByUsername($username);
        if($user)
            return $user;

        throw new UsernameNotFoundException(
            sprintf('Username "%s" does not exist.', $username)
        );
    }

    public function refreshUser(UserInterface $user) {
        return $this->loadUserByUsername($user->getUsername());
    }

    public function supportsClass($class) {
        return User::class === $class;
    }
//HWIOauthBundle
//  public function loadUserByOAuthUserResponse(UserResponseInterface $response){
//
//  }
}