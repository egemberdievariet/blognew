<?php
/**
 * Created by PhpStorm.
 * User: Ariet
 * Date: 4/8/20
 * Time: 6:33 PM
 */

namespace App\DataFixtures\ORM;


use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\ORMFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Term;

class TermLoad extends Fixture{

    public function load(ObjectManager $manager)
    {
        for ($i=1 ; $i<=3; $i++){
            $term = new Term();
            $term->setName("Term".$i);
            $term->setDescription('DESCRIPTION'.$i);
            $manager->persist($term);
        }
        $manager->flush();

    }
}