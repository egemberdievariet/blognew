<?php
/**
 * Created by PhpStorm.
 * User: Ariet
 * Date: 4/8/20
 * Time: 6:33 PM
 */

namespace App\DataFixtures\ORM;


use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\ORMFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Page;
use App\DataFixtures\ORM\TermLoad;
use App\Entity\Term;

class PageLoad extends Fixture{

    public function load(ObjectManager $manager)
    {
        $termRepo = $manager->getRepository(Term::class);
        for ($i=1 ; $i<=3; $i++){
            $page = new Page();
            $page->setTitle('Page'.$i);
            $page->setBody('Body Page'. $i);
            $term = $termRepo->findOneByName('Term'.$i);
            if ($term){
                $page->setCategory($term);
            }
            $page->setCreated(new \DateTime());
            $manager->persist($page);
        }
        $manager->flush();
    }

    public function getDependencies() {
        return [
          TermLoad::class
        ];
     }
}