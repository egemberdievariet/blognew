<?php
/**
 * Created by PhpStorm.
 * User: Ariet
 * Date: 4/17/20
 * Time: 5:27 PM
 */

namespace App\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Role;

class RoleLoad implements FixtureInterface {

    public function load(ObjectManager $manager) {
        $roleRepo = $manager->getRepository(Role::class);
        $role = $roleRepo->findByRole('ROLE_USER');
        if(!$role){
            $role = new Role();
            $role->setName("ROLE USER");
            $role->setRole("ROLE_USER");
            $manager->persist($role);
            $manager->flush();
        }
    }
    public function getOrder(){
        return 1;
    }
}